const db = require('../configs/db');

const moment = require('moment');
const Message = require('../models/Message');
const { default: axios } = require('axios');

exports.send = async function (user, message) {
    const message_url = 'https://hookb.in/pzMMWGwXXbURPnrrPp18';

    axios.post(message_url, {
        message : message
    });

    try {
         await Message.create({
            'user'  : user,
            'message': message,
            'status': 'sended'
         });
        
        return true;
    } catch (error) {
        await Message.create({
            'user'  : user,
            'message': message,
            'status': 'unsended'
        });
    }
    return false;
}

exports.log = function(){
    
}

exports.getUnsended = function(){
    
}

exports.blastBirthdayMessage = function(users){
    let sended = 0;
    users.forEach(element => {
        let message = element.first_name + ' ' + element.last_name +  ', It\'s Is Your Birthday!';

        let isSended = this.send(element.id, message);
        
        if (isSended) {
            sended++;
        }
    });
    
    return sended + ' Message Sended!';
}

