const db = require('../configs/db');

const moment = require('moment');
const { user } = require('../controller/UserController');
const User = require('../models/User');
const { QueryTypes, Sequelize, where } = require("sequelize");


exports.userById =  async function (id) {
    let user =  await User.findByPk(id);

    return user;
}
 
exports.createUser = async function (input) {
    // Try to get user location or guess it (eg : Asia/Bangkok )
    let user_location = (input.location) ? input.location :  moment.tz.guess();

    // Parse the location timezone to timezone format (eg : +07:00)
    let user_timeZone = moment(new Date).tz(user_location).format('Z');

    let created_user = await User.create({
        'first_name' : input.first_name,
        'last_name' : input.last_name,
        'birth_date' : input.birth_date,
        'location': user_location,
        'timezone' : user_timeZone
    });

    return created_user;
}

exports.delete = async function (id) {
    let deleted = await User.destroyshows({
        where: {
            id : id
        }
    });

    if (deleted){
        return 'User Deleted!';
    }

    return 'Can\'t Delete User!';
}

exports.put = async function (id, input) {
    let updated = await User.update(
        {
            first_name: input.first_name,
            last_name: input.last_name,
            birth_date: input.birth_date,
            location: input.location
        }, //what going to be updated
        { where: { id: id } }
    );

    if (updated) {
        return 'User Updated!';
    }

    return 'Can\'t Update User!'
}

exports.getUserTodayBirthday = async function () {
    const users = await db.query("SELECT * FROM `usersByTodayBirthday`", { type: QueryTypes.SELECT });
    return users;
}