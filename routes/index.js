var express = require('express');
var router = express.Router();
const app = express();

const UserController = require('../controller/UserController');
const CronController = require('../controller/CronController');
const { json } = require('body-parser');

app.use(express.json()); // parsing POST ke Json

express.application.prefix = express.Router.prefix = function (path, configure) {
    var router = express.Router();
    this.use(path, router);
    configure(router);
    return router;
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('', { title: 'Express' });
});

router.get('/user/:id', UserController.user);
router.post('/user', UserController.create);
router.delete('/user/:id', UserController.delete);
router.put('/user/:id', UserController.put);

router.get('/user_today_birthday', UserController.getUserTodayBirthday);

router.post('/cron/blast_birthday_message', CronController.blastBirthdayMessage);

module.exports = router;
