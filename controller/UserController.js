const Response = require('./helpers/Response');
const UserService = require('../services/UserService');

const axios = require('axios');
const { json } = require('body-parser');
const db = require('../configs/db');

exports.user = async function (req, res) {
    let id = req.params.id;
    let users = await  UserService.userById(id);
    let data = Response.succesWithData(users);

    res.send(data);
 }

exports.create = async function (req, res) {
    const body = req.body;

    let input = {
        "first_name" : body.first_name,
        "last_name" : body.last_name,
        "birth_date" : body.birth_date,
        "location" : body.location
    };

    createdUser = await UserService.createUser(input);

    let data = Response.succesWithData(createdUser);
    res.send(data);
}

exports.delete = async function (req, res) {
    const id = req.params.id;

    let deleted = await UserService.delete(id);

    let data = Response.succesWithMessage(deleted);
    res.send(data);
}

exports.put = async function (req, res) {
    const id = req.params.id;
    const body = req.body;
    let input = {
        "first_name" : body.first_name,
        "last_name" : body.last_name,
        "birth_date" : body.birth_date,
        "location" : body.location
    };

    let updated = await UserService.put(id,input);

    let data = Response.succesWithMessage(updated);
    res.send(data);
}
 
exports.getUserTodayBirthday = async function (req,res){
    const users = await UserService.getUserTodayBirthday();
    
    let data = Response.succesWithData(users);
    res.send(data);
}
 