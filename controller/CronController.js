const Response = require('./helpers/Response');

const UserService = require('../services/UserService');
const MessageService = require('../services/MessageService');

const axios = require('axios');
const { json } = require('body-parser');
const db = require('../configs/db');

exports.blastBirthdayMessage = async function (req, res) {
    let user_today_birthday = await UserService.getUserTodayBirthday();

    let blast_message = MessageService.blastBirthdayMessage(user_today_birthday);
    
    let data = Response.succesWithMessage(blast_message);
    res.send(data);
}
 