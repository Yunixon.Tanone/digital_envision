exports.succesWithMessage = function(_message = ""){
    const data = {
        code : 1,
        error : null,
        message : _message,
        data : null
    };

    return data;
}

exports.errorWithMessage = function(_message = ""){
    const data = {
        code : 0,
        error : null,
        message : _message,
        data : null
    };

    return data;
}

exports.succesWithData = function(_data = ""){
    const data = {
        code : 1,
        error : null,
        message : null,
        data : _data
    };

    return data;
}

exports.errorWithData = function(_data = null){
    const data = {
        code : 0,
        error : null,
        message : null,
        data : _data
    };

    return data;
}

