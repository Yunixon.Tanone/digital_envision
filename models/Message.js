const sequelize = require('sequelize');
const { mountpath } = require('../app');
const db = require('../configs/db');

const Result = db.define('messages', {
   'id': {
      type: sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
   },
   'user': { type: sequelize.CHAR },
   'message': { type: sequelize.CHAR },
   'status': { type: sequelize.CHAR },
   'updated_at' : {type : sequelize.DATE}
}, {
   timestamps: false
});
 

module.exports = Result;
