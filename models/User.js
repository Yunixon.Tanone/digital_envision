const sequelize = require('sequelize');
const { mountpath } = require('../app');
const db = require('../configs/db');

const Result = db.define('users', {
   'id': {
      type: sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
   },
   'first_name': { type: sequelize.CHAR },
   'last_name': { type: sequelize.CHAR },
   'birth_date': { type: sequelize.DATE },
   'location': { type: sequelize.CHAR },
   'timezone': { type: sequelize.CHAR },
   'updated_at' : {type : sequelize.DATE}
}, {
   timestamps: false
});
 

module.exports = Result;
