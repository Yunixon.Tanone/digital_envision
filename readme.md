## How to use

- Import database
- run npm install
- run npm start

## Endpoints

- POST /user
  <br> Create New User

```json
{
  "first_name": "Yunixon",
  "last_name": "Tanone",
  "birth_date": "2020-04-06",
  "location": "Asia/Bangkok"
}
```

- GET /user/:id
  <br> Get user By Id

- DELETE /user:id
  <br> Delete User By Id

- GET /user
  <br> Get All listed User

- PUT /user/:id
  <br> Update user with given id

```json
{
  "first_name": "Nix",
  "last_name": "Tanone",
  "birth_date": "2020-11-05",
  "location": ""
}
```

- POST /cron/blast_birthday_message
  <br> Get all the users whose birthday is today and blast birthday message
